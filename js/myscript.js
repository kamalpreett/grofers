var items = [{
    "name": "mango",
    "cat": "fruit",
    "price": "100"
}, {
    "name": "pineapple",
    "cat": "fruit",
    "price": "150"
}, {
    "name": "banana",
    "cat": "fruit",
    "price": "20"
}, {
    "name": "blueberry",
    "cat": "fruit",
    "price": "2000"
}, {
    "name": "papaya",
    "cat": "fruit",
    "price": "100"
}, {
    "name": "blackcurrant",
    "cat": "fruit",
    "price": "1000"
}, {
    "name": "avocado",
    "cat": "fruit",
    "price": "1000"
}, {
    "name": "kiwi",
    "cat": "fruit",
    "price": "50"
}, {
    "name": "orange",
    "cat": "fruit",
    "price": "40"
}, {
    "name": "peach",
    "cat": "fruit",
    "price": "60"
}, {
    "name": "broccoli",
    "cat": "vegetable",
    "price": "300"
}, {
    "name": "cabbage",
    "cat": "vegetable",
    "price": "100"
}, {
    "name": "potato",
    "cat": "vegetable",
    "price": "100"
}, {
    "name": "carrot",
    "cat": "vegetable",
    "price": "100"
}, {
    "name": "tomato",
    "cat": "vegetable",
    "price": "100"
}, {
    "name": "turnip",
    "cat": "vegetable",
    "price": "150"
}, {
    "name": "pumpkin",
    "cat": "vegetable",
    "price": "300"
}, {
    "name": "onion",
    "cat": "vegetable",
    "price": "200"
}, {
    "name": "mushroom",
    "cat": "vegetable",
    "price": "500"
}, {
    "name": "capsicum",
    "cat": "vegetable",
    "price": "200"
}, {
    "name": "oreo",
    "cat": "biscuit",
    "price": "35"
}, {
    "name": "goodday",
    "cat": "biscuit",
    "price": "20"
}, {
    "name": "parleg",
    "cat": "biscuit",
    "price": "10"
}, {
    "name": "tiger",
    "cat": "biscuit",
    "price": "20"
}, {
    "name": "crackjack",
    "cat": "biscuit",
    "price": "20"
}, {
    "name": "monaco",
    "cat": "biscuit",
    "price": "20"
}, {
    "name": "treat",
    "cat": "biscuit",
    "price": "30"
}, {
    "name": "darkfantasy",
    "cat": "biscuit",
    "price": "35"
}, {
    "name": "nutrichoice",
    "cat": "biscuit",
    "price": "50"
}, {
    "name": "bounce",
    "cat": "biscuit",
    "price": "10"
}, {
    "name": "milk",
    "cat": "diary",
    "price": "25"
}, {
    "name": "cheese",
    "cat": "diary",
    "price": "100"
}, {
    "name": "curd",
    "cat": "diary",
    "price": "25"
}, {
    "name": "butter",
    "cat": "diary",
    "price": "30"
}, {
    "name": "icecream",
    "cat": "diary",
    "price": "100"
}, {
    "name": "dessert",
    "cat": "diary",
    "price": "100"
}, {
    "name": "custard",
    "cat": "diary",
    "price": "250"
}, {
    "name": "milkshake",
    "cat": "diary",
    "price": "100"
}, {
    "name": "yogurt",
    "cat": "diary",
    "price": "100"
}, {
    "name": "avocadodip",
    "cat": "diary",
    "price": "300"
}];

var searchTable = document.getElementById("search_table");
var input = document.getElementById("search");
var searchItems = [];
var cartItems = [];
var cartTable = document.getElementById("cart_table");
var checkoutTable = document.getElementById("checkout_table");

function search() {
    deleteTable(searchTable);
    searchItems = [];
    if (input.value != "") {
        var tHeadingRow = document.createElement("tr");
        var th1 = document.createElement("th");
        var tName = document.createTextNode("Name");
        th1.appendChild(tName);
        tHeadingRow.appendChild(th1);
        var th2 = document.createElement("th");
        var tCat = document.createTextNode("Category");
        th2.appendChild(tCat);
        tHeadingRow.appendChild(th2);
        var th3 = document.createElement("th");
        var tPrice = document.createTextNode("Price");
        th3.appendChild(tPrice);
        tHeadingRow.appendChild(th3);
        var th4 = document.createElement("th");
        var tQuantity = document.createTextNode("Quantity");
        th4.appendChild(tQuantity);
        tHeadingRow.appendChild(th4);
        searchTable.appendChild(tHeadingRow);

        var pattern = new RegExp("[A-za-z]*" + input.value + "[A-Za-z]*");
        var quantity;
        items.forEach((item) => {
            if (pattern.test(item.name)) {
                var tr = document.createElement("tr");
                var td1 = document.createElement("td");
                var name = document.createTextNode(item.name);
                td1.appendChild(name);
                tr.appendChild(td1);
                var td2 = document.createElement("td");
                var cat = document.createTextNode(item.cat);
                td2.appendChild(cat);
                tr.appendChild(td2);
                var td3 = document.createElement("td");
                var price = document.createTextNode(item.price);
                td3.appendChild(price);
                tr.appendChild(td3);
                quantity = document.createElement("input");
                quantity.setAttribute("type", "number");
                quantity.setAttribute("min", "0");
                quantity.classList.add("quantity");
                tr.appendChild(quantity);
                searchTable.appendChild(tr);
                searchItems.push(item);
            }
        })
    }
}

function reset() {
    deleteTable(searchTable);
    deleteTable(cartTable);
    document.getElementById("search").value = "";
    search_results = [];
    cartItems = [];
}

function deleteTable(table) {
    while (table.hasChildNodes()) {
        table.removeChild(searchTable.firstChild);
    }
}

function add() {
    // deleteTable(cartTable);
    //cartItems = [];
    if (searchItems != "") {
        for (i = 0; i < searchItems.length; i++) {
            var quantity = document.querySelectorAll(".quantity")[i].value;
            if (quantity > 0) {
                var tr = document.createElement("tr");
                var td1 = document.createElement("td");
                var name = document.createTextNode(searchItems[i].name);
                td1.appendChild(name);
                tr.appendChild(td1);
                var td2 = document.createElement("td");
                var itemQuantity = document.createTextNode(quantity);
                td2.classList.add("cart_item_quantity");
                td2.appendChild(itemQuantity);
                tr.appendChild(td2);
                cartTable.appendChild(tr);
                cartItems.push(searchItems[i]);
            }
        }
    }
}

var total1 = 0;

function checkout() {
    // for (i = 0; i < cartItems.length; i++) {
    for (i = checkoutTable.rows.length-1; i < cartItems.length; i++) {

        var quantity = document.querySelectorAll(".cart_item_quantity")[i].innerHTML;
        var tr = document.createElement("tr");
        var td1 = document.createElement("td");
        var name = document.createTextNode(cartItems[i].name);
        td1.appendChild(name);
        tr.appendChild(td1);
        var td2 = document.createElement("td");
        var cat = document.createTextNode(cartItems[i].cat);
        td2.appendChild(cat);
        tr.appendChild(td2);
        var td3 = document.createElement("td");
        var quantity = document.createTextNode(quantity);
        td3.appendChild(quantity);
        tr.appendChild(td3);
        var td4 = document.createElement("td");
        var mrp = document.createTextNode(cartItems[i].price);
        td4.appendChild(mrp);
        tr.appendChild(td4);
        var td5 = document.createElement("td");
        var itemPrice = (cartItems[i].price) * (quantity).data;
        var price = document.createTextNode(itemPrice);
        td5.appendChild(price);
        tr.appendChild(td5);
        checkoutTable.appendChild(tr);
        total1 += itemPrice;
    }
    document.getElementById("total").innerHTML = "";
    var total = document.getElementById("total");
    var para = document.createElement("p");
    para.style.fontSize = "22";
    var text = document.createTextNode("Total is " + total1);
    para.appendChild(text);
    total.appendChild(para);
}






